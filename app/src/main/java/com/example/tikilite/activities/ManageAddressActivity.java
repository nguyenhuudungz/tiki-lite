package com.example.tikilite.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tikilite.helpers.Data;
import com.example.tikilite.helpers.Listener;
import com.example.tikilite.R;
import com.example.tikilite.adapters.AddressAdapter;
import com.example.tikilite.models.Address;

public class ManageAddressActivity extends BaseActivity {

    private static final int REQUEST_ADD = 102;
    private static final int REQUEST_CHG = 100;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_address);
        recyclerView = findViewById(R.id.addr_recycler);

        initToolbar();
        init_recycler_view();
    }

    private void init_recycler_view() {
        Intent intent = new Intent(this, AddAddressActivity.class);
        AddressAdapter adapter = new AddressAdapter();
        adapter.setListener(new Listener() {
            @Override
            public void onClick(int position) {
            }

            @Override
            public void onClick_Detail(int catID, int proID) {
                switch (proID) {
                    case R.id.addr_default:
                        for (int i = 0; i < Data.ADDRESS_LIST.size(); i++) {
                            if (i != catID) {
                                Data.ADDRESS_LIST.get(i).setDefaults(false);
                            } else {
                                Data.ADDRESS_LIST.get(i).setDefaults(true);
                            }
                        }
                        break;
                    case R.id.addr_update:
                        intent.putExtra("address", Data.ADDRESS_LIST.get(catID));
                        startActivityForResult(intent, REQUEST_CHG);
                        break;
                }
                init_recycler_view();
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    public void onAddAddress(View view) {
        Intent intent = new Intent(this, AddAddressActivity.class);
        intent.putExtra("address", new Address());
        startActivityForResult(intent, REQUEST_ADD);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_ADD:
                if (resultCode == Activity.RESULT_OK) {
                    Address address = (Address) data.getSerializableExtra("address");
                    if (address.isDefaults()) {
                        for (Address addr : Data.ADDRESS_LIST) {
                            addr.setDefaults(false);
                        }
                    }

                    Data.ADDRESS_LIST.add(address);
                    init_recycler_view();
                }
                break;
            case REQUEST_CHG:
                if (resultCode == Activity.RESULT_OK) {
                    Address address = (Address) data.getSerializableExtra("address");
                    if (address.isDefaults()) {
                        for (int i = 0; i < Data.ADDRESS_LIST.size(); i++) {
                            if (i != address.getId()) {
                                Data.ADDRESS_LIST.get(i).setDefaults(false);
                            }
                        }
                    }

                    Data.ADDRESS_LIST.set(address.getId(), address);
                    init_recycler_view();
                }
                break;
            default:
                Toast.makeText(this, "Unknown request code", Toast.LENGTH_SHORT).show();
        }
    }
}
