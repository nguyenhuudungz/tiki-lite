package com.example.tikilite.activities;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tikilite.R;
import com.example.tikilite.adapters.SearchHistoryAdapter;
import com.example.tikilite.helpers.Data;
import com.example.tikilite.helpers.Listener;

public class SearchActivity extends AppCompatActivity implements Listener {

    LinearLayout searchHistorySection;
    private SearchView searchView;
    private RecyclerView searchHistoryRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        bindViews();

        // Init searchView
        searchView.setIconified(false);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(new ComponentName(this, SearchResultsActivity.class)));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Data.SEARCH_HISTORY.add(0, query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchHistoryRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        updateAdapter();
    }

    private void updateAdapter() {
        SearchHistoryAdapter adapter = new SearchHistoryAdapter();
        adapter.setCardClickListener(this);
        searchHistoryRecyclerView.setAdapter(adapter);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        updateAdapter();
    }

    private void bindViews() {
        searchView = findViewById(R.id.search_view);
        searchHistoryRecyclerView = findViewById(R.id.search_history_recycler_view);
        searchHistorySection = findViewById(R.id.search_history_section);
    }

    public void onCartClicked(View view) {
        Toast.makeText(this, "onCartClicked", Toast.LENGTH_SHORT).show();
    }

    public void onBackArrowClicked(View view) {
        super.onBackPressed();
    }

    public void onClear(View view) {
        searchView.setQuery("", false);
    }

    public void onSearchKeywordClicked(View view) {
        String query = ((Button) view).getText().toString();
        searchView.setQuery(query, true);
    }

    @Override
    public void onClick(int position) {
        String query = Data.SEARCH_HISTORY.get(position);
        searchView.setQuery(query, true);
    }

    @Override
    public void onClick_Detail(int catID, int proID) {

    }

    public void onSearchHot(View view) {
        String query = ((Button) view).getText().toString();
        searchView.setQuery(query, true);
    }

    public void onClearKeyword(View view) {
        Data.SEARCH_HISTORY.clear();
        updateAdapter();
    }
}
