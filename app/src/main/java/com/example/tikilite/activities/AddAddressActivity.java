package com.example.tikilite.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.example.tikilite.R;
import com.example.tikilite.models.Address;

public class AddAddressActivity extends BaseActivity {

    private int id = -1;
    private EditText editTextName;
    private EditText editTextPhone;
    private EditText editTextAddr;
    private CheckBox checkBoxDefault;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);

        initToolbar();
        Intent intent = getIntent();
        Address address = (Address) intent.getSerializableExtra("address");
        id = address.getId();

        editTextName = findViewById(R.id.editTextName);
        editTextPhone = findViewById(R.id.editTextPhone);
        editTextAddr = findViewById(R.id.editTextAddr);
        checkBoxDefault = findViewById(R.id.checkBoxDefault);

        editTextName.setText(address.getName());
        editTextPhone.setText(address.getPhone());
        editTextAddr.setText(address.getAddress());
        if (address.isDefaults()) {
            checkBoxDefault.setChecked(true);
        }
    }

    public void onAddAddress(View view) {
        String name = editTextName.getText().toString();
        String addr = editTextAddr.getText().toString();
        String phone = editTextPhone.getText().toString();
        boolean defaults = checkBoxDefault.isChecked();

        Address address = new Address(name, addr, phone, defaults);
        if (id >= 0) {
            address.setId(id);
        }

        Intent intent = new Intent();
        intent.putExtra("address", address);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}
