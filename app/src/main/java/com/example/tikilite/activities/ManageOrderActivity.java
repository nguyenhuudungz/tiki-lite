package com.example.tikilite.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tikilite.helpers.Listener;
import com.example.tikilite.R;
import com.example.tikilite.adapters.OrderDetailAdapter;

public class ManageOrderActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_order);
        initToolbar();

        RecyclerView recycler = findViewById(R.id.order_recycler);
        OrderDetailAdapter adapter = new OrderDetailAdapter();
        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setAdapter(adapter);

        adapter.setListener(new Listener() {
            @Override
            public void onClick(int position) {
                Intent intent = new Intent(getApplicationContext(), OrderDetailsActivity.class);
                intent.putExtra("position", position);
                startActivity(intent);
            }

            @Override
            public void onClick_Detail(int catID, int proID) {
            }
        });
    }
}
