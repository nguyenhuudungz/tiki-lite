package com.example.tikilite.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.tikilite.R;
import com.example.tikilite.fragments.AccountPageFragment;
import com.example.tikilite.fragments.CategoryPageFragment;
import com.example.tikilite.fragments.HomePageFragment;
import com.example.tikilite.fragments.SearchPageFragment;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    private Button searchButton;
    private TextView searchHint;
    private TextView textViewUserTab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TabLayout tabLayout = findViewById(R.id.tabs);
        textViewUserTab = findViewById(R.id.text_view_user_tab);
        ViewPager viewPager = findViewById(R.id.pager);
        searchButton = findViewById(R.id.search_button);
        searchHint = findViewById(R.id.search_hint);

        SectionsPagerAdapter pagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 3) {
                    searchButton.setVisibility(View.GONE);
                    searchHint.setVisibility(View.GONE);
                    textViewUserTab.setVisibility(View.VISIBLE);
                } else {
                    searchButton.setVisibility(View.VISIBLE);
                    searchHint.setVisibility(View.VISIBLE);
                    textViewUserTab.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tabLayout.setupWithViewPager(viewPager);
        textViewUserTab.setVisibility(View.GONE);
    }

    public void onCartClicked(View view) {
        Intent intent = new Intent(getApplicationContext(), ShoppingCartActivity.class);
        startActivity(intent);
    }

    public void onSearchHintClick(View view) {
        startSearchActivity();
    }

    private void startSearchActivity() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            // The number of pages in the ViewPager
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getText(R.string.home_tab);
                case 1:
                    return getResources().getText(R.string.category_tab);
                case 2:
                    return getResources().getText(R.string.search_tab);
                case 3:
                    return getResources().getText(R.string.account_tab);
                default:
                    return null;
            }
        }

        @Override
        public Fragment getItem(int position) {
            // The fragment to be displayed on each page
            switch (position) {
                case 0:
                    return new HomePageFragment();
                case 1:
                    return new CategoryPageFragment();
                case 2:
                    return new SearchPageFragment();
                case 3:
                    return new AccountPageFragment();
                default:
                    return null;
            }
        }
    }

    public void manageOrder(View view) {
        Intent intent = new Intent(this, ManageOrderActivity.class);
        startActivity(intent);
    }

    public void manageAddr(View view) {
        Intent intent = new Intent(this, ManageAddressActivity.class);
        startActivity(intent);
    }
}
