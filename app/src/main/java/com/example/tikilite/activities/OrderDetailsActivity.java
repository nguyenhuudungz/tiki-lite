package com.example.tikilite.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tikilite.helpers.Data;
import com.example.tikilite.R;
import com.example.tikilite.adapters.CheckOutViewAdapter;
import com.example.tikilite.models.Order;
import com.example.tikilite.models.OrderDetail;

import java.text.SimpleDateFormat;

public class OrderDetailsActivity extends BaseActivity {

    private Order order;
    private OrderDetail detail;
    private RecyclerView recyclerView;

    private TextView order_id;
    private TextView order_date;
    private TextView order_status;

    private TextView textViewName;
    private TextView textViewPhone;
    private TextView textViewAddr;

    private TextView textViewSubFee;
    private TextView textViewShipFee;
    private TextView textViewTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        Intent intent = getIntent();
        int orderID = intent.getIntExtra("position", 0);
        detail = Data.DETAIL_LIST.get(orderID);
        order = Data.ORDER_LIST.get(orderID);

        initToolbar();
        init_view_comp();
        init_view_data();
    }

    private void init_view_comp() {
        order_id = findViewById(R.id.order_id);
        order_date = findViewById(R.id.order_date);
        order_status = findViewById(R.id.order_status);

        textViewName = findViewById(R.id.textViewName);
        textViewPhone = findViewById(R.id.textViewPhone);
        textViewAddr = findViewById(R.id.textViewAddr);

        textViewSubFee = findViewById(R.id.textViewSubFee);
        textViewShipFee = findViewById(R.id.textViewShipFee);
        textViewTotal = findViewById(R.id.textViewTotal);
        recyclerView = findViewById(R.id.check_out_view_recycler);
    }

    private void init_view_data() {
        order_id.setText("Mã đơn hàng: " + order.getOrderId());
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        order_date.setText("Ngày đặt hàng: " + dateFormat.format(order.getOrderDate()));

        switch (order.getStatus()) {
            case DELIVERED:
                order_status.setText("Trạng thái: Giao hàng thành công");
                break;
            case PROGRESSING:
                order_status.setText("Trạng thái: Đang giao hàng");
                break;
            case CANCELLED:
                order_status.setText("Trạng thái: Đã huỷ");
                break;
            default:
        }

        textViewName.setText(detail.getAddress().getName());
        textViewPhone.setText(detail.getAddress().getPhone());
        textViewAddr.setText(detail.getAddress().getAddress());

        CheckOutViewAdapter adapter = new CheckOutViewAdapter(detail.getProductList());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        String sub = String.format("%,d đ", detail.getSubTotal());
        textViewSubFee.setText(sub);
        String ship = String.format("%,d đ", Data.SHIP_FEE);
        textViewShipFee.setText(ship);
        String total = String.format("%,d đ", detail.getSubTotal() + Data.SHIP_FEE);
        textViewTotal.setText(total);
    }
}
