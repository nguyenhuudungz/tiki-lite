package com.example.tikilite.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tikilite.helpers.Data;
import com.example.tikilite.R;
import com.example.tikilite.adapters.CartProductAdapter;
import com.example.tikilite.helpers.Listener;
import com.example.tikilite.models.Product;

public class ShoppingCartActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private TextView total_price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_cart);
        initToolbar();

        recyclerView = findViewById(R.id.cart_recycler_view);
        total_price = findViewById(R.id.total_price);
        init_cart_data();
    }

    private int getTotalPrice() {
        int sum = 0;
        for (Product product : Data.CART.getProductList()) {
            sum += product.getPrice();
        }

        Data.CART.setSubTotal(sum);
        return sum;
    }

    private void init_cart_data() {
        CartProductAdapter adapter = new CartProductAdapter();
        adapter.setListener(new Listener() {
            @Override
            public void onClick(int position) {
                Data.CART.getProductList().remove(position);
                init_cart_data();
            }

            @Override
            public void onClick_Detail(int catID, int proID) {
            }
        });

        setTitle("Giỏ hàng (" + Data.CART.getProductList().size() + ")");
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        String total = String.format("%,d đ", getTotalPrice());
        total_price.setText(total);
    }

    public void onOrder(View view) {
        Intent intent = new Intent(this, CheckOutAddrActivity.class);
        startActivity(intent);
    }
}
