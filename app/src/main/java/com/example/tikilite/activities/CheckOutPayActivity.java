package com.example.tikilite.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.tikilite.R;

import java.util.Calendar;

public class CheckOutPayActivity extends BaseActivity {

    private TextView textViewShipDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out_pay);

        initToolbar();
        textViewShipDate = findViewById(R.id.textViewShipDate);
        textViewShipDate.setText(get_ship_date());
    }

    private String get_ship_date() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 5);
        String days[] = {"Chủ nhật, ", "Thứ hai, ", "Thứ ba, ", "Thứ tư, ", "Thứ năm, ", "Thứ sáu, ", "Thứ bảy, "};
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        return "Giao vào " + days[dayOfWeek] + calendar.get(Calendar.DATE) + "/" + calendar.get(Calendar.MONTH);
    }

    public void onCont(View view) {
        Intent intent = new Intent(this, CheckOutViewActivity.class);
        startActivity(intent);
    }
}
