package com.example.tikilite.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.tikilite.R;
import com.example.tikilite.helpers.Data;
import com.example.tikilite.models.Order;
import com.example.tikilite.models.OrderDetail;

import java.util.ArrayList;
import java.util.Date;

public class CheckOutDoneActivity extends AppCompatActivity {

    private TextView order_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out_done);

        String name = Data.CART.getProductList().get(0).getName();
        Data.ORDER_LIST.add(0, new Order(name, new Date(), Order.Status.PROGRESSING));
        Data.DETAIL_LIST.add(0, Data.CART);
        Data.CART = new OrderDetail(null, new ArrayList<>(), 0);

        order_id = findViewById(R.id.order_id);
        order_id.setText(String.valueOf(Data.ORDER_LIST.get(0).getOrderId()));
    }

    public void onCont(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void seeOrder(View view) {
        Intent intent = new Intent(this, OrderDetailsActivity.class);
        intent.putExtra("position", 0);
        startActivity(intent);
    }
}
