package com.example.tikilite.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tikilite.R;
import com.example.tikilite.adapters.CheckOutAddrAdapter;
import com.example.tikilite.helpers.Data;
import com.example.tikilite.helpers.Listener;
import com.example.tikilite.models.Address;
import com.kofigyan.stateprogressbar.StateProgressBar;

public class CheckOutAddrActivity extends BaseActivity {

    private int address_checked;
    private static final int REQUEST_ADD = 102;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out_addr);
        recyclerView = findViewById(R.id.check_out_addr_recycler);

        initToolbar();
        init_recycler_view();

        String[] descriptionData = {"Địa chỉ", "Thanh toán", "Xác nhận"};
        StateProgressBar stateProgressBar = findViewById(R.id.scroll_bar);
        stateProgressBar.setStateDescriptionData(descriptionData);
    }

    private void init_recycler_view() {
        CheckOutAddrAdapter adapter = new CheckOutAddrAdapter();
        adapter.setListener(new Listener() {
            @Override
            public void onClick(int position) {
                address_checked = position;
            }

            @Override
            public void onClick_Detail(int catID, int proID) {
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    public void onShip(View view) {
        Data.CART.setAddress(Data.ADDRESS_LIST.get(address_checked));
        Intent intent = new Intent(this, CheckOutPayActivity.class);
        startActivity(intent);
    }

    public void onAdd(View view) {
        Intent intent = new Intent(this, AddAddressActivity.class);
        intent.putExtra("address", new Address());
        startActivityForResult(intent, REQUEST_ADD);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_ADD:
                if (resultCode == Activity.RESULT_OK) {
                    Address address = (Address) data.getSerializableExtra("address");
                    if (address.isDefaults()) {
                        for (Address addr : Data.ADDRESS_LIST) {
                            addr.setDefaults(false);
                        }
                    }

                    Data.ADDRESS_LIST.add(address);
                    init_recycler_view();
                }
                break;
            default:
                Toast.makeText(this, "Unknown request code", Toast.LENGTH_SHORT).show();
        }
    }
}
