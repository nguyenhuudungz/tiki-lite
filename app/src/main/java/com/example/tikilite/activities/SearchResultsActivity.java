package com.example.tikilite.activities;

import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tikilite.R;
import com.example.tikilite.adapters.ProductAdapter;
import com.example.tikilite.helpers.Data;
import com.example.tikilite.helpers.Listener;
import com.example.tikilite.models.Product;

import java.util.ArrayList;
import java.util.List;

public class SearchResultsActivity extends BaseActivity implements Listener {

    List<Product> products = new ArrayList<>();
    private RecyclerView productsRecyclerView;
    private int selectedSortChoice = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);
        initToolbar();
        bindView();
        handleIntent(getIntent());
    }

    private void updateData() {
        productsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        ProductAdapter adapter = new ProductAdapter(products);
        adapter.setClickListener(this);
        productsRecyclerView.setAdapter(adapter);
    }

    private void bindView() {
        productsRecyclerView = findViewById(R.id.products_recycler_view);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            Bundle extras = intent.getBundleExtra("extras");
            if (extras != null && extras.getInt("mode") == 1) {
                String query = extras.getString("query");
                setTitle("Tìm kiếm: " + query);
                products = Data.getProductListByCat(query);
                updateData();
            } else {
                String query = intent.getStringExtra(SearchManager.QUERY);
                setTitle("Tìm kiếm: " + query);
                products = Data.getProductListByName(query);
                updateData();
            }
        }
    }

    @Override
    public void onClick(int position) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("product", products.get(position));
        startActivity(intent);
    }

    @Override
    public void onClick_Detail(int catID, int proID) {

    }

    public void onSortClicked(View view) {
        String[] items = {"Chọc lọc", "Hàng mới", "Bán chạy", "Giảm giá nhiều", "Giá thấp", "Giá cao"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle("Sắp xếp")
                .setSingleChoiceItems(items, selectedSortChoice, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectedSortChoice = which;
                        dialog.cancel();
                    }
                })
                .show();
    }
}
