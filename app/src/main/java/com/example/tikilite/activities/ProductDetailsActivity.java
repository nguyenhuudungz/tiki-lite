package com.example.tikilite.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tikilite.helpers.Data;
import com.example.tikilite.helpers.DownloadImageTask;
import com.example.tikilite.R;
import com.example.tikilite.models.Product;

public class ProductDetailsActivity extends BaseActivity {

    private Product product;
    private ImageView product_image;
    private TextView product_name;
    private TextView product_price;
    private TextView product_category;
    private TextView description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        initToolbar();
        Intent intent = getIntent();
        product = (Product) intent.getSerializableExtra("product");

        product_image = findViewById(R.id.product_image);
        product_name = findViewById(R.id.product_name);
        product_price = findViewById(R.id.product_price);
        product_category = findViewById(R.id.product_category);
        description = findViewById(R.id.description);

        new DownloadImageTask(product_image).execute(product.getImageUrl());
        product_name.setText(product.getName());
        String price = String.format("%,d đ", (int) product.getPrice());
        product_price.setText(price);
        product_category.setText(product.getCategory());
        description.setText(product.getDescription());
    }

    public void onAdd(View view) {
        Data.CART.getProductList().add(product);
        Toast.makeText(this, "Product add successfully", Toast.LENGTH_SHORT).show();
    }
}
