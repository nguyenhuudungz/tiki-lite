package com.example.tikilite.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tikilite.R;
import com.example.tikilite.adapters.CheckOutViewAdapter;
import com.example.tikilite.helpers.Data;
import com.kofigyan.stateprogressbar.StateProgressBar;

public class CheckOutViewActivity extends BaseActivity {

    private TextView textViewName;
    private TextView textViewPhone;
    private TextView textViewAddr;

    private TextView textViewSubFee;
    private TextView textViewShipFee;
    private TextView textViewTotal;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out_view);

        initToolbar();
        init_view_comp();
        init_view_data();
    }

    private void init_view_comp() {
        textViewName = findViewById(R.id.textViewName);
        textViewPhone = findViewById(R.id.textViewPhone);
        textViewAddr = findViewById(R.id.textViewAddr);

        textViewSubFee = findViewById(R.id.textViewSubFee);
        textViewShipFee = findViewById(R.id.textViewShipFee);
        textViewTotal = findViewById(R.id.textViewTotal);

        recyclerView = findViewById(R.id.check_out_view_recycler);
        String[] descriptionData = {"Địa chỉ", "Thanh toán", "Xác nhận"};
        StateProgressBar stateProgressBar = findViewById(R.id.scroll_bar);
        stateProgressBar.setStateDescriptionData(descriptionData);
    }

    private void init_view_data() {
        textViewName.setText(Data.CART.getAddress().getName());
        textViewPhone.setText(Data.CART.getAddress().getPhone());
        textViewAddr.setText(Data.CART.getAddress().getAddress());

        CheckOutViewAdapter adapter = new CheckOutViewAdapter(Data.CART.getProductList());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        String sub = String.format("%,d đ", Data.CART.getSubTotal());
        textViewSubFee.setText(sub);
        String ship = String.format("%,d đ", Data.SHIP_FEE);
        textViewShipFee.setText(ship);
        String total = String.format("%,d đ", Data.CART.getSubTotal() + Data.SHIP_FEE);
        textViewTotal.setText(total);
    }

    public void onPaid(View view) {
        Intent intent = new Intent(this, CheckOutDoneActivity.class);
        startActivity(intent);
    }
}
