package com.example.tikilite.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.tikilite.helpers.Data;
import com.example.tikilite.R;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ManageAccountActivity extends BaseActivity {

    private SimpleDateFormat dateFormat;
    private DatePickerDialog datePicker;

    private EditText editTextName;
    private EditText editTextEmail;
    private EditText editTextDob;
    private RadioButton radioButtonMale;

    private CheckBox checkBoxChange;
    private EditText editTextOldPass;
    private EditText editTextNewPass;
    private EditText editTextRePass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_account);

        initToolbar();
        init_view_comp();
        custom_view();
    }

    private void init_view_comp() {
        editTextName = findViewById(R.id.editTextName);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextDob = findViewById(R.id.editTextDob);
        radioButtonMale = findViewById(R.id.radioButtonMale);

        checkBoxChange = findViewById(R.id.checkBoxChange);
        editTextOldPass = findViewById(R.id.editTextOldPass);
        editTextNewPass = findViewById(R.id.editTextNewPass);
        editTextRePass = findViewById(R.id.editTextRePass);

        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        editTextName.setText(Data.USER.getName());
        editTextEmail.setText(Data.USER.getEmail());

        String dob = dateFormat.format(Data.USER.getDob());
        editTextDob.setText(dob);
        radioButtonMale.setChecked(Data.USER.isMale());
        set_change_pass(View.GONE);
    }

    private void set_change_pass(int vis) {
        editTextOldPass.setVisibility(vis);
        editTextNewPass.setVisibility(vis);
        editTextRePass.setVisibility(vis);
    }

    private void custom_view() {
        checkBoxChange.setOnCheckedChangeListener((buttonView, isChecked) -> {
            set_change_pass(isChecked ? View.VISIBLE : View.GONE);
        });

        DatePickerDialog.OnDateSetListener callback = (view, year, month, dayOfMonth) -> {
            Date date = new Date(year - 1900, month, dayOfMonth);
            editTextDob.setText(dateFormat.format(date));
        };

        Date dob = Data.USER.getDob();
        datePicker = new DatePickerDialog(this, AlertDialog.THEME_HOLO_LIGHT,
                callback, dob.getYear() + 1900, dob.getMonth(), dob.getDate());
        datePicker.setTitle("Chọn ngày sinh");
        editTextDob.setOnClickListener(v -> datePicker.show());
    }

    private void finish_activity() {
        try {
            Date date = dateFormat.parse(editTextDob.getText().toString());
            Data.USER.setDob(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Data.USER.setName(editTextName.getText().toString());
        Data.USER.setMale(radioButtonMale.isChecked());
        setResult(Activity.RESULT_OK, new Intent());
        finish();
    }

    public void onSave(View view) {
        if (checkBoxChange.isChecked()) {
            String oldPass = editTextOldPass.getText().toString();
            String newPass = editTextNewPass.getText().toString();
            String rePass = editTextRePass.getText().toString();

            if (oldPass.compareTo(Data.USER.getPassword()) != 0) {
                Toast.makeText(this, "Old password is wrong", Toast.LENGTH_SHORT).show();
            } else if (newPass.compareTo(rePass) != 0) {
                Toast.makeText(this, "New password is not match", Toast.LENGTH_SHORT).show();
            } else {
                Data.USER.setPassword(newPass);
                finish_activity();
            }
        } else {
            finish_activity();
        }
    }
}
