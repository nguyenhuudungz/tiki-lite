package com.example.tikilite.models;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Order implements Serializable {

    private static int s_id = 2810;

    private int orderId;
    private String name;
    private Date orderDate;
    private Status status;

    public Order(String name, Date orderDate, Status status) {
        this.orderId = s_id++;
        this.name = name;
        this.orderDate = orderDate;
        this.status = status;
    }

    public enum Status {
        DELIVERED,
        CANCELLED,
        PROGRESSING
    }
}
