package com.example.tikilite.models;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Product implements Serializable {

    private static int s_id = 0;

    private int id;
    private String category;
    private String name;
    private long price;
    private String description;
    private String imageUrl;
    private int review;

    public Product(String category, String name, long price, String description, String imageUrl, int review) {
        this.id = s_id++;
        this.category = category;
        this.name = name;
        this.price = price;
        this.description = description;
        this.imageUrl = imageUrl;
        this.review = review;
    }
}
