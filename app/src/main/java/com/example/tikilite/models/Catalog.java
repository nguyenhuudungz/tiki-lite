package com.example.tikilite.models;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Catalog implements Serializable {

    private static int s_id = 0;

    private int id;
    private String title;
    private String imageUrl;
    private int more;
    private List<Product> list;

    public Catalog(String title, String imageUrl, int more, List<Product> list) {
        this.id = s_id++;
        this.title = title;
        this.imageUrl = imageUrl;
        this.more = more;
        this.list = list;
    }
}
