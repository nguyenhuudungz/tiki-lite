package com.example.tikilite.models;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class User implements Serializable {

    private String name;
    private String imageUrl;
    private String email;
    private Date fromDate;

    private Date dob;
    private boolean male;
    private String password;

    public User(String name, String imageUrl, String email, Date fromDate, Date dob, boolean male, String password) {
        this.name = name;
        this.imageUrl = imageUrl;
        this.email = email;
        this.fromDate = fromDate;
        this.dob = dob;
        this.male = male;
        this.password = password;
    }
}
