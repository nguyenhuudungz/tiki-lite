package com.example.tikilite.models;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class OrderDetail implements Serializable {

    private static int s_id = 0;

    private int orderID;
    private Address address;
    private List<Product> productList;
    private int subTotal;

    public OrderDetail(Address address, List<Product> productList, int subTotal) {
        this.orderID = s_id++;
        this.address = address;
        this.productList = productList;
        this.subTotal = subTotal;
    }
}
