package com.example.tikilite.models;

import java.io.Serializable;

import lombok.Data;

@Data
public class Address implements Serializable {

    private static int s_id = 0;

    private int id;
    private String name;
    private String address;
    private String phone;
    private boolean defaults;

    public Address() {
        id = -1;
    }

    public Address(String name, String address, String phone, boolean defaults) {
        this.id = s_id++;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.defaults = defaults;
    }
}
