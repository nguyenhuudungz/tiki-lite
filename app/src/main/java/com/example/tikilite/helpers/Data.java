package com.example.tikilite.helpers;

import com.example.tikilite.models.Address;
import com.example.tikilite.models.Order;
import com.example.tikilite.models.OrderDetail;
import com.example.tikilite.models.Product;
import com.example.tikilite.models.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Data {

    public static final String CAT1 = "Hải sản";
    public static final String CAT2 = "Rau an toàn";
    public static final String CAT3 = "Trái cây";
    public static final String CAT4 = "Thịt cá dân dã";
    public static final String CAT5 = "Hàng khô";
    public static final String CAT6 = "Rau hữu cơ";
    public static final String CAT7 = "Thực phẩm chế biến sẵn";
    public static final String CAT8 = "Khác";
    public static final String IMG_CAT1 = "https://product.hstatic.net/1000191320/product/thu_tuoi_09d6984a0fd14bb5847b2886799b077f_grande.jpg";
    public static final String IMG_CAT2 = "https://product.hstatic.net/1000191320/product/ban-hat-giong-rau-cai-lan-cuahanghatgiong_grande.jpg";
    public static final String IMG_CAT3 = "https://product.hstatic.net/1000191320/product/7eb870a7a4407d96effa815ce8fa4a75_grande.jpg";
    public static final String IMG_CAT4 = "https://product.hstatic.net/1000191320/product/thit-nac-vai-luon-que2_grande.jpg";
    public static final String IMG_CAT5 = "https://product.hstatic.net/1000191320/product/superfoods-hartley-walnuts_grande.jpg";
    public static final String IMG_CAT6 = "https://product.hstatic.net/1000191320/product/cai-mo_grande.jpg";
    public static final String IMG_CAT7 = "https://product.hstatic.net/1000191320/product/28378964-919167581580205-6812274824196562331-n_grande.jpg";
    public static final String IMG_CAT8 = "https://product.hstatic.net/1000191320/product/18527604_1675279605823234_3821535703042271230_n_2e621c449941490d88d06f69eb067c36_grande.jpg";
    public static final String AVATAR = "https://live.staticflickr.com/2409/2440790012_6603ec8acd_z.jpg";
    public static final int SHIP_FEE = 18000;
    public static final String DESCRIP = "What is Lorem Ipsum?\n" +
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n" +
            "\n" +
            "Why do we use it?\n" +
            "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\n" +
            "\n" +
            "Where does it come from?\n" +
            "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.\n" +
            "\n" +
            "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.";

    public static List<String> SEARCH_HISTORY = new ArrayList<>();
    public static List<Product> PRODUCT_LIST = new ArrayList<>();
    public static List<Address> ADDRESS_LIST = new ArrayList<>();
    public static List<Order> ORDER_LIST = new ArrayList<>();
    public static List<OrderDetail> DETAIL_LIST = new ArrayList<>();
    public static OrderDetail CART = null;
    public static User USER = null;

    static {
        PRODUCT_LIST.add(new Product(CAT1, "Cá nâu", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/decc63710018e746be09_661333f8415c40b89d781d05913bfaf8_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT1, "Cua đồng xay nhỏ", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/cua-dong-xay_807fb4b24508491ea2f2e84c4f7908b9_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT1, "Cá bống đục", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/ca-bong-duc_dat-la_2_af361d6c69ff427b93acdbeea4782b84_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT1, "Cá thát lát rút xương", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/9495967924254_29d48654446b406d99bf7bbe04c57e7c_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT1, "Cá Lăng cắt khúc", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/product_250110090233_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT1, "Cua nâu Nauv", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/cua_bac_au_1_600x_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT1, "Mực trứng Phú Quốc", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/vao-bep-che-bien-mon-muc-trung-cho-bua-com-gia-dinh-them-hap-dan-1_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT1, "Mực ống Phú Quốc", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/muc-ong_5c991ef7481e43d4b754f3668091e678_grande.jpg", rReview()));

        PRODUCT_LIST.add(new Product(CAT2, "Khoai sọ", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/upload_d8e7e66eb722447db459b7fcd8f4c02c_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT2, "Cá rốt", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/c__r_t_nh_t_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT2, "Cải thảo", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/caithao_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT2, "Rau mùi", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/rau_mui_tri_tan_nhang_1__grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT2, "Rau ngót", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/rau_ngot_v_600x_2x_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT2, "Ngọc su su", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/ngon-su-su_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT2, "Hành lá", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/hanh-la_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT2, "Đậu Hà Lan", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/9570164244510_grande.jpg", rReview()));

        PRODUCT_LIST.add(new Product(CAT3, "Nho Ba Moi", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/nho_b94a6939e71641549036f0764a79dfb7_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT3, "Kiwi vàng", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/1-v_f1cc64e4d1e64a809c4ee38170852c00_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT3, "Dưa hấu", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/dua_hau__38a61351381c4baeb510094ee63eba4a_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT3, "Đu đủ", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/44531250_1973489372686778_763281002066870272_n_e6ec690a1ae844ba8372b5e938404fc7_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT3, "Sầu riêng", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/aa60bf2b3994ddca8485_6376e052701f4dc9addbd72bc37429c8_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT3, "Na bở", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/na_bo_5cd7a321a97a4f73a69ee6ee4ef2cf35_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT3, "Bưởi da xanh", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/product3141_f1d7f33d079648dabbccf65b8c09c325_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT3, "Dưa lê Hàn Quốc", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/dua-le-han-quoc-502x502_0c45c66d78d44a77a8c613aca272abbf_grande.jpg", rReview()));

        PRODUCT_LIST.add(new Product(CAT4, "Vịt quế", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/vit_que_6eec9a4dc61c433985da7da266e2b7a6_grande_20f2f967e8404e95b9a5d2521f924569_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT4, "Thịt bê Nghệ An", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/be_c324f3b7ba2f41bca559d5531bbbb3bc_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT4, "Gà đen Hà Giang", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/gd1_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT4, "Ngan nguyên con", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/ngan-bac-tom_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT4, "Cá quả nguyên con", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/z1155175062925_e285a17f23157a9df840db36d4b81639_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT4, "Gà quế", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/chicken-alive_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT4, "Gà đồi Hòa Bình", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/recipe_cover_r27661_811cf258af3841b5ad053da4df72b176_grande.jpg", rReview()));
        PRODUCT_LIST.add(new Product(CAT4, "Chim câu Hòa Bình", rPrice(), DESCRIP, "https://product.hstatic.net/1000191320/product/chim-b_-c_u_grande.jpg", rReview()));

        CART = new OrderDetail(null, new ArrayList<>(), 0);
        USER = new User("Hữu Dũng", AVATAR, "omega1100100@gmail.com", new Date(2015 - 1900, 10, 28),
                new Date(1998 - 1900, 11, 4), true, "1");

        ADDRESS_LIST.add(new Address("Nguyễn Hữu Dũng", "Số nhà 157, Đường Lý Thái Tổ, Phường Trần Phú, TP Bắc Giang, " +
                "Tỉnh Bắc Giang", "0359263711", false));
        ADDRESS_LIST.add(new Address("Nguyễn Hữu Dũng", "Cây xăng 39, Thôn 8, Xã Thạch Hòa, Huyện Thạch Thất, TP Hà " +
                "Nội", "0359263711", true));
        ADDRESS_LIST.add(new Address("Nguyễn Hữu Dũng", "Số nhà 20, Ngõ 20, Đường Thành Công, Phường Thành Công, Quận " +
                "Ba Đình, TP Hà Nội", "0383994708", false));

        ORDER_LIST.add(new Order(PRODUCT_LIST.get(1).getName(), new Date(2019 - 1900, 10, 7), Order.Status.DELIVERED));
        ORDER_LIST.add(new Order(PRODUCT_LIST.get(9).getName(), new Date(2019 - 1900, 10, 8), Order.Status.CANCELLED));
        ORDER_LIST.add(new Order(PRODUCT_LIST.get(17).getName(), new Date(2019 - 1900, 10, 9), Order.Status.PROGRESSING));

        List<Product> list1 = new ArrayList<>();
        list1.add(PRODUCT_LIST.get(1));
        list1.add(PRODUCT_LIST.get(8));
        long sub1 = PRODUCT_LIST.get(1).getPrice() + PRODUCT_LIST.get(8).getPrice();
        DETAIL_LIST.add(new OrderDetail(ADDRESS_LIST.get(0), list1, (int) sub1));

        List<Product> list2 = new ArrayList<>();
        list2.add(PRODUCT_LIST.get(9));
        list2.add(PRODUCT_LIST.get(16));
        long sub2 = PRODUCT_LIST.get(9).getPrice() + PRODUCT_LIST.get(16).getPrice();
        DETAIL_LIST.add(new OrderDetail(ADDRESS_LIST.get(1), list2, (int) sub2));

        List<Product> list3 = new ArrayList<>();
        list3.add(PRODUCT_LIST.get(17));
        list3.add(PRODUCT_LIST.get(24));
        long sub3 = PRODUCT_LIST.get(17).getPrice() + PRODUCT_LIST.get(24).getPrice();
        DETAIL_LIST.add(new OrderDetail(ADDRESS_LIST.get(2), list3, (int) sub3));

        Collections.reverse(ORDER_LIST);
        Collections.reverse(DETAIL_LIST);
    }

    public static List<Product> getProductListByCat(String cat) {
        return PRODUCT_LIST.stream().filter(p -> p.getCategory().toLowerCase().equals(cat.toLowerCase())).collect(Collectors.toList());
    }

    public static List<Product> getProductListByName(String name) {
        return PRODUCT_LIST.stream().filter(p -> p.getName().toLowerCase().contains(name.toLowerCase())).collect(Collectors.toList());
    }

    public static int rReview() {
        return Math.abs(new Random().nextInt() % (int) 10000);
    }

    public static long rPrice() {
        return 123000L + Math.abs(new Random().nextLong() % (int) 1000) * 1000;
    }
}
