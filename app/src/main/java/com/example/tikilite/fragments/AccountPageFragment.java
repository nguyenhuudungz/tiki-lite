package com.example.tikilite.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.tikilite.helpers.Data;
import com.example.tikilite.helpers.DownloadImageTask;
import com.example.tikilite.R;
import com.example.tikilite.activities.ManageAccountActivity;

import java.text.SimpleDateFormat;

public class AccountPageFragment extends Fragment {

    private static final int REQUEST_CHG = 100;
    private LinearLayout linearLayoutUser;

    private ImageView imageViewUser;
    private TextView textViewName;
    private TextView textViewEmail;
    private TextView textViewFrom;

    public AccountPageFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_account_page, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        imageViewUser = getView().findViewById(R.id.imageViewUser);
        textViewName = getView().findViewById(R.id.textViewName);
        textViewEmail = getView().findViewById(R.id.textViewEmail);
        textViewFrom = getView().findViewById(R.id.textViewFrom);

        init_fragment_data();
        linearLayoutUser = getView().findViewById(R.id.linearLayoutUser);
        linearLayoutUser.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), ManageAccountActivity.class);
            startActivityForResult(intent, REQUEST_CHG);
        });
    }

    private void init_fragment_data() {
        new DownloadImageTask(imageViewUser).execute(Data.USER.getImageUrl());
        textViewName.setText(Data.USER.getName());
        textViewEmail.setText(Data.USER.getEmail());

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String date = dateFormat.format(Data.USER.getFromDate());
        textViewFrom.setText("Thành viên từ " + date);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_CHG:
                if (resultCode == Activity.RESULT_OK) {
                    init_fragment_data();
                }
                break;
            default:
                Toast.makeText(getActivity(), "Unknown request code", Toast.LENGTH_SHORT).show();
        }
    }
}
