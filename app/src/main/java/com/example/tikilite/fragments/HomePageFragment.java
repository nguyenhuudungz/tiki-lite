package com.example.tikilite.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tikilite.activities.SearchResultsActivity;
import com.example.tikilite.helpers.Data;
import com.example.tikilite.helpers.Listener;
import com.example.tikilite.R;
import com.example.tikilite.activities.ProductDetailsActivity;
import com.example.tikilite.adapters.CatalogAdapter;
import com.example.tikilite.models.Catalog;

import java.util.ArrayList;
import java.util.List;

public class HomePageFragment extends Fragment {

    private List<Catalog> catalogList = new ArrayList<>();
    private RecyclerView recyclerView;

    public HomePageFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_page, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        catalogList.add(new Catalog(Data.CAT1, "https://m.soibien.vn/media/img/product/365/03/banner%20phi%20thuc%20pham_ee01534928357.jpg", Data.rReview(), Data.getProductListByCat(Data.CAT1)));
        catalogList.add(new Catalog(Data.CAT2, "https://cdn.missfresh.com/web/3.26.5/images/box.png", Data.rReview(), Data.getProductListByCat(Data.CAT2)));
        catalogList.add(new Catalog(Data.CAT3, "https://m.soibien.vn/media/img/product/365/03/banner%20web_85e1540870374_13e1543395494.jpg", Data.rReview(), Data.getProductListByCat(Data.CAT3)));
        catalogList.add(new Catalog(Data.CAT4, "https://soibien.vn/homefresh/image/Image_1.jpg", Data.rReview(), Data.getProductListByCat(Data.CAT4)));

        recyclerView = getView().findViewById(R.id.catalog_recycler);
        CatalogAdapter adapter = new CatalogAdapter(catalogList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        adapter.setListener(new Listener() {
            @Override
            public void onClick(int position) {
                // for search intent
                Intent intent = new Intent(getActivity(), SearchResultsActivity.class);
                Bundle extras = new Bundle();
                extras.putString("query", catalogList.get(position).getTitle());
                extras.putInt("mode", 1);
                intent.setAction(Intent.ACTION_SEARCH).putExtra("extras", extras);
                startActivity(intent);
            }

            @Override
            public void onClick_Detail(int catID, int proID) {
                Intent intent = new Intent(getActivity(), ProductDetailsActivity.class);
                intent.putExtra("product", catalogList.get(catID).getList().get(proID));
                startActivity(intent);
            }
        });
    }
}
