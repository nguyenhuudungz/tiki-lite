package com.example.tikilite.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import com.example.tikilite.R;
import com.example.tikilite.activities.SearchResultsActivity;
import com.example.tikilite.helpers.Data;
import com.example.tikilite.helpers.DownloadImageTask;

public class CategoryPageFragment extends Fragment {

    public CategoryPageFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_category_page, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        init_image_view(R.id.imageViewCat1, Data.IMG_CAT1, Data.CAT1);
        init_image_view(R.id.imageViewCat2, Data.IMG_CAT2, Data.CAT2);
        init_image_view(R.id.imageViewCat3, Data.IMG_CAT3, Data.CAT3);
        init_image_view(R.id.imageViewCat4, Data.IMG_CAT4, Data.CAT4);
        init_image_view(R.id.imageViewCat5, Data.IMG_CAT5, Data.CAT5);
        init_image_view(R.id.imageViewCat6, Data.IMG_CAT6, Data.CAT6);
        init_image_view(R.id.imageViewCat7, Data.IMG_CAT7, Data.CAT7);
        init_image_view(R.id.imageViewCat8, Data.IMG_CAT8, Data.CAT8);
    }

    private void init_image_view(int imageID, String imageUrl, String name) {
        ImageView imageView = getView().findViewById(imageID);
        imageView.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), SearchResultsActivity.class);
            Bundle extras = new Bundle();
            extras.putString("query", name);
            extras.putInt("mode", 1);
            intent.setAction(Intent.ACTION_SEARCH).putExtra("extras", extras);
            startActivity(intent);
        });
        new DownloadImageTask(imageView).execute(imageUrl);
    }
}
