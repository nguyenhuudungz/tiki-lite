package com.example.tikilite.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tikilite.helpers.Data;
import com.example.tikilite.helpers.DownloadImageTask;
import com.example.tikilite.R;
import com.example.tikilite.helpers.Listener;
import com.example.tikilite.models.Product;

public class CartProductAdapter extends RecyclerView.Adapter<CartProductAdapter.ViewHolder> {

    Listener listener;

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        CardView card = (CardView) inflater.inflate(R.layout.cart_product_card, parent, false);
        return new ViewHolder(card);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Product product = Data.CART.getProductList().get(position);
        CardView card = holder.card;
        Button removeButton = card.findViewById(R.id.remove_button);
        removeButton.setOnClickListener(v -> listener.onClick(position));

        ImageView image = card.findViewById(R.id.cart_product_image);
        TextView name = card.findViewById(R.id.cart_product_name);
        TextView price = card.findViewById(R.id.cart_product_price);

        new DownloadImageTask(image).execute(product.getImageUrl());
        name.setText(product.getName());
        String prs = String.format("%,d đ", (int) product.getPrice());
        price.setText(prs);
    }

    @Override
    public int getItemCount() {
        return Data.CART.getProductList().size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        CardView card;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            card = itemView.findViewById(R.id.cart_product_cardview);
        }
    }
}
