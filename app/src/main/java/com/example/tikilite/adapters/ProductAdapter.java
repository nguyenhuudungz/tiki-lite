package com.example.tikilite.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tikilite.helpers.Data;
import com.example.tikilite.helpers.DownloadImageTask;
import com.example.tikilite.helpers.Listener;
import com.example.tikilite.R;
import com.example.tikilite.models.Product;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    private List<Product> productList;
    private Listener listener;

    public ProductAdapter(List<Product> productList) {
        this.productList = productList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        CardView view = (CardView) inflater.inflate(R.layout.product_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Product product = productList.get(position);
        CardView card = holder.cardView;
        card.setOnClickListener(v -> listener.onClick(position));

        ImageView product_image = card.findViewById(R.id.product_image);
        TextView product_name = card.findViewById(R.id.product_name);
        TextView product_price = card.findViewById(R.id.product_price);
        TextView product_review = card.findViewById(R.id.product_review);

        new DownloadImageTask(product_image).execute(product.getImageUrl());
        product_name.setText(product.getName());
        product_price.setText(String.valueOf(product.getPrice()));
        product_review.setText("Reviews: " + product.getReview());
    }

    public void setClickListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.product_card_view);
        }
    }
}
