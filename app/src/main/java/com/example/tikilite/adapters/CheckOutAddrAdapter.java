package com.example.tikilite.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tikilite.helpers.Data;
import com.example.tikilite.R;
import com.example.tikilite.helpers.Listener;
import com.example.tikilite.models.Address;

import java.util.List;

public class CheckOutAddrAdapter extends RecyclerView.Adapter<CheckOutAddrAdapter.ViewHolder> {

    private Listener listener;
    private RadioButton radioButton;

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        CardView view = (CardView) inflater.inflate(R.layout.check_out_addr_card, parent, false);
        return new CheckOutAddrAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        Address address = Data.ADDRESS_LIST.get(position);
        holder.textViewName.setText(address.getName());
        holder.textViewPhone.setText(address.getPhone());
        holder.textViewAddr.setText(address.getAddress());

        if (radioButton == null && address.isDefaults()) {
            holder.radioButtonChoose.setChecked(true);
            radioButton = holder.radioButtonChoose;
        }

        holder.radioButtonChoose.setOnClickListener(v -> {
            listener.onClick(position);
            radioButton.setChecked(false);
            holder.radioButtonChoose.setChecked(true);
            radioButton = holder.radioButtonChoose;
        });
    }

    @Override
    public int getItemCount() {
        return Data.ADDRESS_LIST.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private RadioButton radioButtonChoose;
        private TextView textViewName;
        private TextView textViewPhone;
        private TextView textViewAddr;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.radioButtonChoose = itemView.findViewById(R.id.radioButtonChoose);
            this.textViewName = itemView.findViewById(R.id.textViewName);
            this.textViewPhone = itemView.findViewById(R.id.textViewPhone);
            this.textViewAddr = itemView.findViewById(R.id.textViewAddr);
        }
    }
}
