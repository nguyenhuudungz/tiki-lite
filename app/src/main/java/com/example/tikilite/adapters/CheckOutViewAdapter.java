package com.example.tikilite.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tikilite.helpers.DownloadImageTask;
import com.example.tikilite.R;
import com.example.tikilite.models.Product;

import java.util.List;

public class CheckOutViewAdapter extends RecyclerView.Adapter<CheckOutViewAdapter.ViewHolder> {

    private List<Product> productList;

    public CheckOutViewAdapter(List<Product> productList) {
        this.productList = productList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        CardView view = (CardView) inflater.inflate(R.layout.check_out_view_card, parent, false);
        return new CheckOutViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        Product product = productList.get(position);
        new DownloadImageTask(holder.imageView).execute(product.getImageUrl());
        holder.textViewName.setText(product.getName());

        holder.textViewCat.setText(product.getCategory());
        String price = String.format("%,d đ", product.getPrice());
        holder.textViewPrice.setText(price);
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private TextView textViewName;
        private TextView textViewCat;
        private TextView textViewPrice;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.imageView = itemView.findViewById(R.id.imageView);
            this.textViewName = itemView.findViewById(R.id.textViewName);
            this.textViewCat = itemView.findViewById(R.id.textViewCat);
            this.textViewPrice = itemView.findViewById(R.id.textViewPrice);
        }
    }
}
