package com.example.tikilite.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tikilite.helpers.DownloadImageTask;
import com.example.tikilite.helpers.Listener;
import com.example.tikilite.R;
import com.example.tikilite.models.Catalog;
import com.example.tikilite.models.Product;

import java.util.ArrayList;
import java.util.List;

public class CatalogAdapter extends RecyclerView.Adapter<CatalogAdapter.ViewHolder> {

    private List<Catalog> catalogList;
    private Listener listener;

    public CatalogAdapter(List<Catalog> catalogList) {
        this.catalogList = catalogList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        CardView view = (CardView) inflater.inflate(R.layout.catalog_card, parent, false);
        return new CatalogAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Catalog catalog = catalogList.get(position);
        holder.textViewTitle.setText(catalog.getTitle());
        new DownloadImageTask(holder.imageView).execute(catalog.getImageUrl());
        holder.imageView.setOnClickListener(v -> listener.onClick(position));
        String more = String.format("XEM THÊM %,d SẢN PHẨM >", catalog.getMore());
        holder.textViewMore.setText(more);

        for (int i = 0; i < 8; i++) {
            Product product = catalog.getList().get(i);
            ProductView view = holder.productList.get(i);

            view.textViewName.setText(product.getName());
            new DownloadImageTask(view.imageView).execute(product.getImageUrl());
            String price = String.format("%,d đ", product.getPrice());
            view.textViewPrice.setText(price);

            final int proID = i;
            view.imageView.setOnClickListener(v -> listener.onClick_Detail(position, proID));
        }
    }

    @Override
    public int getItemCount() {
        return catalogList.size();
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView textViewTitle;
        private ImageView imageView;
        private TextView textViewMore;
        private List<ProductView> productList;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.textViewTitle = itemView.findViewById(R.id.textViewTitle);
            this.imageView = itemView.findViewById(R.id.imageView);
            this.textViewMore = itemView.findViewById(R.id.textViewMore);

            this.productList = new ArrayList<>();
            this.productList.add(new ProductView(itemView, R.id.textViewName1, R.id.imageView1, R.id.textViewPrice1));
            this.productList.add(new ProductView(itemView, R.id.textViewName2, R.id.imageView2, R.id.textViewPrice2));
            this.productList.add(new ProductView(itemView, R.id.textViewName3, R.id.imageView3, R.id.textViewPrice3));
            this.productList.add(new ProductView(itemView, R.id.textViewName4, R.id.imageView4, R.id.textViewPrice4));
            this.productList.add(new ProductView(itemView, R.id.textViewName5, R.id.imageView5, R.id.textViewPrice5));
            this.productList.add(new ProductView(itemView, R.id.textViewName6, R.id.imageView6, R.id.textViewPrice6));
            this.productList.add(new ProductView(itemView, R.id.textViewName7, R.id.imageView7, R.id.textViewPrice7));
            this.productList.add(new ProductView(itemView, R.id.textViewName8, R.id.imageView8, R.id.textViewPrice8));
        }
    }

    public static class ProductView {

        private TextView textViewName;
        private ImageView imageView;
        private TextView textViewPrice;

        public ProductView(View view, int name, int image, int price) {
            this.textViewName = view.findViewById(name);
            this.imageView = view.findViewById(image);
            this.textViewPrice = view.findViewById(price);
        }
    }
}
