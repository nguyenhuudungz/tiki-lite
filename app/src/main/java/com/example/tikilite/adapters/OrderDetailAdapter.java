package com.example.tikilite.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tikilite.helpers.Data;
import com.example.tikilite.helpers.Listener;
import com.example.tikilite.R;
import com.example.tikilite.models.Order;

import java.text.SimpleDateFormat;

public class OrderDetailAdapter extends RecyclerView.Adapter<OrderDetailAdapter.ViewHolder> {

    private Listener listener;

    @NonNull
    @Override
    public OrderDetailAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                            int viewType) {
        CardView cv = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_manage_order_card, parent, false);
        return new ViewHolder(cv);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderDetailAdapter.ViewHolder holder,
                                 final int position) {
        CardView cardView = holder.cardView;
        TextView order_title = cardView.findViewById(R.id.order_title);
        TextView order_id = cardView.findViewById(R.id.order_id);
        TextView order_date = cardView.findViewById(R.id.order_date);
        final TextView order_status = cardView.findViewById(R.id.order_status);

        Order order = Data.ORDER_LIST.get(position);
        order_title.setText(order.getName());
        order_id.setText("Mã đơn hàng: " + order.getOrderId());
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        order_date.setText("Ngày đặt hàng: " + dateFormat.format(order.getOrderDate()));

        switch (order.getStatus()) {
            case DELIVERED:
                order_status.setText("Trạng thái: Giao hàng thành công");
                break;
            case PROGRESSING:
                order_status.setText("Trạng thái: Đang giao hàng");
                break;
            case CANCELLED:
                order_status.setText("Trạng thái: Đã huỷ");
                break;
        }

        cardView.setOnClickListener(v -> {
            listener.onClick(position);
        });
    }

    @Override
    public int getItemCount() {
        return Data.ORDER_LIST.size();
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private CardView cardView;

        public ViewHolder(CardView v) {
            super(v);
            this.cardView = v;
        }
    }
}
