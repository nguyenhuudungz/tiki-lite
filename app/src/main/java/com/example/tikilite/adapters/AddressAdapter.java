package com.example.tikilite.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tikilite.helpers.Data;
import com.example.tikilite.helpers.Listener;
import com.example.tikilite.R;
import com.example.tikilite.models.Address;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {

    private Listener listener;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        CardView view = (CardView) inflater.inflate(R.layout.address_card, parent, false);
        return new AddressAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        Address address = Data.ADDRESS_LIST.get(position);
        holder.textViewName.setText(address.getName());
        holder.textViewAddr.setText(address.getAddress());
        holder.textViewPhone.setText(address.getPhone());
        if (address.isDefaults()) {
            holder.textViewDefault.setVisibility(View.VISIBLE);
        } else {
            holder.textViewDefault.setVisibility(View.GONE);
        }

        holder.textViewOption.setOnClickListener(v -> {
            PopupMenu popup = new PopupMenu(v.getContext(), holder.textViewOption);
            popup.inflate(R.menu.menu_address);
            popup.setOnMenuItemClickListener(item -> {
                listener.onClick_Detail(position, item.getItemId());
                return true;
            });
            popup.show();
        });
    }

    @Override
    public int getItemCount() {
        return Data.ADDRESS_LIST.size();
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView textViewName;
        private TextView textViewAddr;
        private TextView textViewPhone;
        private TextView textViewDefault;
        private TextView textViewOption;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.textViewName = itemView.findViewById(R.id.textViewName);
            this.textViewAddr = itemView.findViewById(R.id.textViewAddr);
            this.textViewPhone = itemView.findViewById(R.id.textViewPhone);
            this.textViewDefault = itemView.findViewById(R.id.textViewDefault);
            this.textViewOption = itemView.findViewById(R.id.textViewOption);
        }
    }
}
