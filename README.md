# Tiki Lite

This is the assignment of PRM391.

Deadline: 14/11/2019.

Phase 1 - UI    : 28/10 - 07/11
Phase 2 - Logic : 08/11 - 13/11
Phase 3 - ISC : 14/11

Process cycle:

```
Choose task from 'To do' list ==> Move to 'Doing' ==> Coding ==> Create MR ===ok===> Merge
                                                        ⬆           ⬇
                                                        ⬆         Discuss
                                                        ⬆           ⬇
                                                        ==============
```

Target device is Samsung Galaxy S8+.

[Cấu trúc commit](https://chris.beams.io/posts/git-commit/)